//Initilized both origin and destination maps
function initMap() {
     //Draw map 1
     var montreal = {lat: 45.5017, lng: -73.5673};
     var map1 = new google.maps.Map(document.getElementById('map1'), {
          zoom: 4,
          center: montreal
     });

     //Insert search bar on map 1
     var origin = document.getElementById('map-origin');
     var searchBox = new google.maps.places.SearchBox(origin);
     map1.controls[google.maps.ControlPosition.TOP_RIGHT].push(origin);

     //Draw map 2
     var montreal = {lat: 45.5017, lng: -73.5673};
     var map2 = new google.maps.Map(document.getElementById('map2'), {
          zoom: 4,
          center: montreal
     });

     //Insert search bar on map 2
     var destination = document.getElementById('map-destination');
     var searchBox = new google.maps.places.SearchBox(destination);
     map2.controls[google.maps.ControlPosition.TOP_RIGHT].push(destination);
}


//Finds the distance between the origin and destination using google DistanceMatrixService
function getDistance(){
     var distanceService = new google.maps.DistanceMatrixService();
     distanceService.getDistanceMatrix({
        origins: [$("#map-origin").val()],
        destinations: [$("#map-destination").val()],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        durationInTraffic: true,
        avoidHighways: false,
        avoidTolls: false
     },

    function (response, status) {
        if (status !== google.maps.DistanceMatrixStatus.OK) {
            console.log('Error:', status);
        } else {
          var kilometers = Math.ceil((response.rows[0].elements[0].distance.value) / 1000);
          //console.log(kilometers);
          calculatePrice(kilometers);
        }
     });
}

//Calculates the price of travel between origin and destination
//using distance value obtained from the DistanceMatrixService
//and using pre-defined price values
function calculatePrice(kilometers) {

     var kms = kilometers * 1.5;
     var truck1 = 1.50;
     var truck2 = 1.25;
     var truck3 = 1;
     var cargo1 = 1.1;
     var cargo2 = 1.2;
     var cargo3 = 1.3;
     var cargo4 = 1.4;
     var cargo5 = 1.5;
     var price;

     //Determine which truck is checked
     if($('#radio1').is(':checked')){
          price = kms * truck1;
     }
     else if ($('#radio2').is(':checked')){
          price = kms * truck2;
     }
     else if ($('#radio3').is(':checked')){
          price = kms * truck3;
     }

     //Determine which category of cargo is checked
     if($('#opt1').is(':selected')){
          price *= cargo1;
          textOutput(price);
     }
     else if($('#opt2').is(':selected')){
          price *= cargo2;
          textOutput(price);
     }
     else if($('#opt3').is(':selected')){
          price *= cargo3;
          textOutput(price);
     }
     else if($('#opt4').is(':selected')){
          price *= cargo4;
          textOutput(price);
     }
     else if($('#opt5').is(':selected')){
          price *= cargo5;
          textOutput(price);
     }

     function textOutput(price){
       var prc = price.toFixed(2);
       $('#result').append(prc);

     }
}
